﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using QuadTree;

namespace QuadTreeTests
{
    [TestFixture]
    public class QuadTreeTests
    {
        private QuadTree<TestCell> _tree;
 
        /// <summary>
        /// The node lookup for quick access to nodes in the tree
        /// </summary>
        Dictionary<int, Node<TestCell>> _nodeLookup;
        private static int _cellTotal = 0; // used to generate test cell names
        private int _depth = 2;

        [SetUp]
        public void SetUp() {
            Node<TestCell>.ResetNodeId();
            _cellTotal = 0;
            GenerateTestTree(_depth, 1);
        }

        /// <summary>
        /// Simple test where there are no empty nodes to remove
        /// </summary>
        [Test, Sequential]
        public void DeleteUnused_NoResults() {
            Assert.AreEqual(0, _tree.DeleteUnusedNodes());
        }

        /// <summary>
        /// Delete a leaf, expect one node removed
        /// </summary>
        [Test, Sequential]
        public void DeleteUnused_RemoveSingleLeaf()
        {
            var leafNodesParent = _tree.Descendant(_depth - 1, 0);
            leafNodesParent.Children[0].Items.Clear();
            leafNodesParent.Children[1].Items.Clear();
            leafNodesParent.Children[2].Items.Clear();
            leafNodesParent.Children[3].Items.Clear();
            Assert.AreEqual(4, _tree.DeleteUnusedNodes());
        }

        /// <summary>
        /// Delete two leaf, expect two node removed
        /// </summary>
        [Test, Sequential]
        public void DeleteUnused_RemoveTwoLeaves()
        {
            var leafNodesParent1 = _tree.Descendant(_depth - 1, 0);
            leafNodesParent1.Children[0].Items.Clear();
            leafNodesParent1.Children[1].Items.Clear();
            leafNodesParent1.Children[2].Items.Clear();
            leafNodesParent1.Children[3].Items.Clear();
            var leafNodesParent2 = _tree.Descendant(_depth - 1, 3);
            leafNodesParent2.Children[0].Items.Clear();
            leafNodesParent2.Children[1].Items.Clear();
            leafNodesParent2.Children[2].Items.Clear();
            leafNodesParent2.Children[3].Items.Clear();
            Assert.AreEqual(8, _tree.DeleteUnusedNodes());
        }

        /// <summary>
        /// Delete middle node items, expect no nodes removed
        /// </summary>
        [Test, Sequential]
        public void DeleteUnused_RemoveItems_StillHasChildren()
        {
            var intermediateNode = _nodeLookup[2];
            intermediateNode.Items.Clear();
             Assert.AreEqual(0, _tree.DeleteUnusedNodes());
        }

        /// <summary>
        /// Generates the test tree.
        /// </summary>
        /// <param name="depth">Depth.</param>
        /// <param name="itemsPerNode">Items per node.</param>
        private void GenerateTestTree(int depth, int itemsPerNode) {
            _nodeLookup = new Dictionary<int, Node<TestCell>>();
            _tree = new QuadTree<TestCell>();
            _nodeLookup[_tree.Root.Id] = _tree.Root;

            // Track which nodes were added to the current depth
            var lastNodes = new List<Node<TestCell>>() { _tree.Root };

            // Build tree one level at a time
            for (var curDepth = 1; curDepth <= depth; ++curDepth) {
                var curDepthNodes = lastNodes.ToList();
                lastNodes.Clear();

                // Add 4 new children to the current depth
                foreach(var node in curDepthNodes) {
                     for (var childId = 0; childId < 4; ++childId) {
                        var newNode = new Node<TestCell>(CreateTestCells(itemsPerNode));
                        node.Children[childId] = newNode;
                        _nodeLookup[newNode.Id] = newNode;
                        lastNodes.Add(newNode);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the test cells.
        /// </summary>
        /// <returns>The test cells.</returns>
        /// <param name="count">Count.</param>
        private List<TestCell> CreateTestCells(int count) {
            var testCells = new List<TestCell>();
            for (var i = 0; i < count; ++i) {
                testCells.Add(new TestCell($"TestCell {++_cellTotal}"));
            }
            return testCells;
        }
    }

    public class TestCell {
        public string CellName { get; set; }
        public TestCell(string name) { CellName = name; }
    }
}
