﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuadTree
{
    public class QuadTree<T>
    {
        public Node<T> Root { get; private set; } = new Node<T>();

        public QuadTree()
        {
            
        }

        /// <summary>
        /// Delete branches that are unused. Root always remains intact.
        /// </summary>
        /// <returns>The unused nodes.</returns>
        public int DeleteUnusedNodes() {
            int nodesDeleted = 0;
            DeleteUnusedNodes(Root, ref nodesDeleted);           
            return nodesDeleted;
        }

        /// <summary>
        /// Deletes the unused nodes.
        /// </summary>
        /// <returns><c>true</c>, if this node should be deleted from parent, <c>false</c> otherwise.</returns>
        /// <param name="node">Node.</param>
        /// <param name="nodesDeleted">Nodes deleted.</param>
        private bool DeleteUnusedNodes(Node<T> node, ref int nodesDeleted) {
            if (node == null) return true; // recursion stop

            var allChildrenAreEmpty = true;
            var hasNullChild = false;
            for (var i = 0; i < 4; ++i)
            {
                var child = node.Children[i];
                if (child == null)
                {
                    hasNullChild = true;
                    break;
                }
                if (!DeleteUnusedNodes(child, ref nodesDeleted))
                    allChildrenAreEmpty = false;
            }

            if (!allChildrenAreEmpty)
                return false;

            // Do your recycle thang
            if (!hasNullChild)
            {
                node.Children[0] = null;
                node.Children[1] = null;
                node.Children[2] = null;
                node.Children[3] = null;
                nodesDeleted += 4;
            }
            return !node.Items.Any();
        }

        public Node<T> Descendant(int depth, int childPath)
        {
            return Descendant(depth, childPath, Root);
        }

        /// <summary>
        /// Traverses breadth first always following the child[childPath] where childPath is between 0-3
        /// </summary>
        /// <param name="depth"></param>
        /// <param name="childPath"></param>
        /// <returns></returns>
        private Node<T> Descendant(int depth, int childPath, Node<T> node)
        {
            if (node == null) return null;
            if (depth == 0) return node;
            return Descendant(depth - 1, childPath, node.Children[childPath]);
        }
    }
}
