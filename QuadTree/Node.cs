﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuadTree
{
    public class Node<T>
    {
        public static void ResetNodeId() => _id = 0;
        private static int _id;
        public int Id { get; private set; } = ++_id;
        public List<T> Items { get; private set; } = new List<T>();
        public Node<T>[] Children { get; private set; } = new Node<T>[4];

        public Node(IEnumerable<T> items = null)
        {
            if (items?.Any() == true)
                Items.AddRange(items);
        }

    }
}
